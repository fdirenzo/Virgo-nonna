# This is a tutorial for NonNA, a Virgo Non-stationary Noise Analysis tool.
# Author: Francesco Di Renzo	Last update: 13.06.18
#
# Taken a time interval, the script generates a target sinal 'y' (18-26) and some
# random witness (auxiliary) channels.


import numpy
import scipy
import pylab
from nonna_test_module import *
from scipy.signal import welch, freqz
from math import log, floor
#from scipy import signal

# Initialise random state for reproducibility
numpy.random.seed(42)

#### Generate a time interval for the data

#### Make the target y signal

high_freq = True	# Set true 

if high_freq:
	fs = 2e3 # kHz
	duration = 100

	time = numpy.linspace(0,duration,duration*fs)

	if fs <= 2**10:
		nps = floor(log(fs,2))
	else:
		nps = 2**10

	#nl = butter_bnf_filter(numpy.random.normal(0,10,len(time)),0,10,fs,2,'low')		
	#nh = butter_bnf_filter(numpy.random.normal(0,2,len(time)),fs/10,fs,fs,1,'high')	
	nh = numpy.gradient(numpy.random.normal(0,1,len(time)))		# Generate high-freq noise with propto f PSD
	nl = numpy.cumsum(numpy.random.normal(0,1e-3,len(time)))	# Generate high-freq noise
	nl = numpy.cumsum(nl)
	nl = butter_bnf_filter(nl,1,fs,fs,3,'high')
	nb = numpy.random.normal(0,.4,len(time))											# Generate withe noise
	fiftyHz = 0.1 * numpy.sin(2*numpy.pi*50*time)									# Generate 50 Hz line
	y = nl+nh+nb+fiftyHz															# Add them all
	
	f, yf = welch(y,fs,nperseg=nps)												# Compute their PSD
	
	plot_everything = True
	if plot_everything:
		nlf = welch(nl,fs,nperseg=nps)[1]
		nhf = welch(nh,fs,nperseg=nps)[1]
		nbf = welch(nb,fs,nperseg=nps)[1]
		fHzf = welch(fiftyHz,fs,nperseg=nps)[1]
			

	plot_now = True
	
	if plot_now:
		pylab.ion()
		pylab.figure(1,[16,6])
		pylab.subplot(121)
		pylab.plot(time,y)
#		if plot_everything:
#			pylab.plot(time,nh,alpha=.5,label='High freq. noise')
#			pylab.plot(time,nb,alpha=.5,label='High freq. noise')
#			pylab.plot(time,nl,alpha=.5,label='High freq. noise')
#			pylab.plot(time,fiftyHz,alpha=.5,label='High freq. noise')
		pylab.grid()
		pylab.xlabel('Time [sec]')
		pylab.ylabel('Signals [AU]')
		pylab.subplot(122)
		pylab.loglog(f,yf,linewidth=2,label='Total PSD')
		if plot_everything:
			pylab.loglog(f,nhf,alpha=.5,label='High freq. noise')
			pylab.loglog(f,nbf,alpha=.5,label='Low freq. noise')
			pylab.loglog(f,nlf,alpha=.5,label='Withe noise')
			pylab.loglog(f,fHzf,alpha=.5,label='50 Hz noise')
		pylab.grid(which='both')
		pylab.xlim([10,fs/4])
		pylab.ylim([min(yf)/5,max(yf)*2])
		pylab.xlabel('Frequency [Hz]')
		pylab.ylabel(r'Signals PSD [AU${}^2 $/Hz]')
		pylab.legend(loc='best')

else:
	fs = 10 # kHz
	duration = 100
	if fs <= 2**10:
		nps = floor(log(fs,2))
	else:
		nps = 2**10


	time = numpy.linspace(0,duration,fs*duration)
	
	base = (numpy.sin(20*numpy.pi*time) + 3*numpy.sin(10*numpy.pi*time) - numpy.sin(50*numpy.pi*time) - 3* numpy.sin(30*numpy.pi*time))
	# Withe noise 
	noise1 = numpy.random.normal(6,2*numpy.random.rand(),len(time))
	noise2 = numpy.random.normal(4,4*numpy.random.rand(),len(time))
	noise3 = numpy.random.normal(2,3*numpy.random.rand(),len(time))
	noise4 = numpy.random.normal(0,numpy.random.rand(),len(time))

	y = base + noise1 + noise2 + noise3 + noise4

	f,yf = welch(y,fs)

raw_input('Press any key to continue')
#### Generate the auxes
n_aux = 10

x = numpy.zeros((len(time),n_aux))

for i in range(n_aux):
	randomicity = 8
	R = numpy.random.normal(0,1,randomicity)
	for r in range(1,len(R)):
		x[:,i] = x[:,i] + R[r-1]*(numpy.sin(2*numpy.pi*10*r*R[r]*time) +numpy.random.rand(len(time)))


# Plot y and X for a check
pylab.ion()
pylab.figure(None,[14,6])
pylab.plot(time,x,alpha=0.5)
pylab.plot(time,y,'b',linewidth=2.0,label='target')
pylab.legend(loc='best')
pylab.xlabel('Time [sec]')
pylab.grid()

raw_input('Press any key to continue')


xf = numpy.zeros((len(f),n_aux))
for i in range(n_aux):
	xf[:,i] = welch(x[:,i],fs,nperseg=nps)[1]


pylab.figure()
pylab.loglog(f,xf)
pylab.loglog(f,yf,'b',linewidth=2,label='target')
pylab.xlim([10,fs/4])
pylab.legend(loc='best')
pylab.grid(which='both')
pylab.xlabel('Frequency (Hz)')

raw_input('Press any key to continue')


f_band = [f[0],f[-1]]

filter_test = raw_input('Do you whant to test the Butterworth filter? (y/n)\n')

if filter_test == 'y' or filter_test == None:
		
	f_band[0] = float(raw_input('Choose a lower frequency for band-pass: '))
	f_band[1] = float(raw_input('Choose a higher frequency for band-pass: '))

	pylab.figure()

	if f_band[0]>f[0] and f_band[1]<f[-1]:
		for order in [3, 6, 9]:
    	   		b, a = butter_bandpass(f_band[0],f_band[1], fs, order=order)
    	   		w, h = freqz(b, a, worN=len(time))
     	  		pylab.plot((fs * 0.5 / numpy.pi) * w, abs(h), label="order = %d" % order)
			pylab.title('Band-pass [%d,%d] Butterworth filter' %(f_band[0],f_band[1]))
	
	elif f_band[0]<=f[0] and f_band[1]<f[-1]:
		for order in [3, 6, 9]:
    	   		b, a = butter_lowpass(f_band[1], fs, order=order)
    	   		w, h = freqz(b, a, worN=len(time))
     	  		pylab.plot((fs * 0.5 / numpy.pi) * w, abs(h), label="order = %d" % order)
			pylab.title('Low-pass [0,%d] Butterworth filter' % f_band[1] )

	elif f_band[0]>f[0] and f_band[1]>=f[-1]:
		for order in [3, 6, 9]:
    	   		b, a = butter_highpass(f_band[0], fs, order=order)
    	   		w, h = freqz(b, a, worN=len(time))
     	  		pylab.plot((fs * 0.5 / numpy.pi) * w, abs(h), label="order = %d" % order)
			pylab.title('High-pass [%d,%d] Butterworth filter' % (f_band[0],f[-1]) )

	
		
	pylab.xlabel('Frequency (Hz)')
	pylab.ylabel('Gain')
	pylab.grid(True)
	pylab.legend(loc='best')

	order = raw_input('Choose an order for the Butterworth filter: ')
	try:
		order = int(order)
	except ValueError:
		order = 3
	
	if f_band[0]>f[0] and f_band[1]<f[-1]:
		y_filt = butter_bnf_filter(y, f_band[0], f_band[1], fs, order, btype='band')
	elif f_band[0]<=f[0] and f_band[1]<f[-1]:
		y_filt = butter_bnf_filter(y, f_band[0], f_band[1], fs, order, btype='low')
	elif f_band[0]>f[0] and f_band[1]>=f[-1]:
		y_filt = butter_bnf_filter(y, f_band[0], f_band[1], fs, order, btype='high')
	else:
		y_filt = y
	
	y_filtf = welch(y_filt,fs,nperseg=nps)[1]

	pylab.figure(None,[16,6])
	pylab.subplot(121)
	pylab.plot(time,y,'b',linewidth=2.0)
	pylab.plot(time,y_filt,'r')
	pylab.xlabel('Time (s)')

	pylab.subplot(122)
	pylab.loglog(f,yf,'b',linewidth=2,label="Target")
	pylab.loglog(f,y_filtf,'r',label="order %d bandpass filtered [%.1f,%.1f]" % (2*order,f_band[0],f_band[1]))
	pylab.xlim([10,fs/4])
	pylab.ylim([min(yf)/10,max(yf)*2])
	pylab.grid(which='both')
	pylab.xlabel('Frequency (Hz)')
	pylab.legend(loc='best')


#### Regression analysis

raw_input('\nImplementing the regression analysis...\nPress any key to continue')

# add target to aux
if 'y_filt' in locals():
	how_noisy = 4
	noisy_y_filt = y_filt + numpy.random.normal(how_noisy*numpy.random.rand(),how_noisy*numpy.random.rand(),len(time))
	xc = numpy.hstack((x,numpy.reshape(noisy_y_filt,(len(time),1))))
else:
	xc = numpy.hstack((x,numpy.reshape(y,(len(time),1))))
#xc = x

X = scipy.zeros((xc.shape[0],xc.shape[1]+1))
X[:,:-1] = xc
X[:,-1] = 1
X = scipy.mat(X)

# p = numpy.zeros((X.shape[1],1)) # <- not really needed
p = scipy.linalg.inv(X.T * X) * X.T * scipy.mat(y).T

pylab.figure()
pylab.plot(time,y,'b',linewidth=2,label="Target")
pylab.plot(time,X*p,'r',label="Best prediction")
#pylab.plot(time,X*p-numpy.resize(y,(len(y),1)),'g',label='Residual error')
pylab.plot(time,X*p-y.reshape((len(y),1)),'g',label='Residual error')
pylab.legend(loc='best')
pylab.xlabel('Time (s)')

e,R2 = rerror(X,p,y)
print 'Least-mean-squared regression error: %g\nTarget variance: %g\nGOF (1-LMSE/Var(y)): %.1f%%' %(e,numpy.var(y),R2*100)



#### Noise subtraction

Xpf = welch(numpy.ravel(X*p),fs,nperseg=nps)[1]
ef = welch(numpy.ravel(y.reshape((len(y),1))-X*p),fs,nperseg=nps)[1]

pylab.figure()
pylab.loglog(f,yf,'b',linewidth=2,label="Target")
pylab.loglog(f,Xpf,'r',label='Best prediction')
pylab.loglog(f,ef,'g',label='Residual error')
pylab.grid(which='both')
pylab.xlim([10,fs/4])
pylab.xlabel('Frequency (Hz)')
pylab.legend(loc='best')
