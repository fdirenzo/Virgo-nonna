# Virgo-nonna
#### NOn statioNa Noise Analysis 

NonNA is a Non-stationary Noise Analysis tool first invented by Gabriele Vajente and Tristan Shoemaker. Currently it is supported and developed by Francesco Di Renzo and Jose Maria Gonzalez Castro.
The python script performs a regression analysis of a target channel with a set of auxiliary "predictors" channels. The target can be the Band-Limited RMS (BLRMS) of a signal whitin a certain frequency band or the time series corresponding of the maximum frequencies of a drifting line. In the first case the script computes first the BLRMS of the target and then tries to make the best possible prediction of its slow time variation using the auxiliary channels. In the second case it first produced the time series of the maxima.

________________________________________________
### Basic Structure:

NonNA consists of three main files:
- nonna\_virgo.py, which is the basic script
and two modules:
- nonna\_functions\_og.py, written by Vajente and containing the basic auxiliary functions used by nonna (some of them are no longer used)

- nonna\_get\_data\_virgo.py, which adapts the orginal script tu run on Virgo data and contains some new features.
Also, to run properly, nonna\_virgo.py needs the python virgotools available on Virgo machines at Bas Swinkels' '/user/swinkels/deploy/PythonVirgoTools/trunk/src'.

------------------------------------------------

### Basic instructions:

#### INPUTS:

- target channel, the one we want to clean/predict (Hrec\_hoft\_ , LSC\_DARM) 
- gps start (use the built-in "quick and rough" gps calcualtor)
- data duration in seconds: beware that longer periods are most likely to contain glitches and unlockings
- frequency band where to compute the BL-RMS
- list of auxiliary channels, and list of excluded channels (containing errors etc.)
- other parameters: max order of correlations, frequency sampling for decimation,
  slow variations frequency, outlier threshold.

#### PLOTS:

- target signal BLRMS (downsampled)
- aux channels downsampled (all at once or "verbose" for inspection)
- target BLRMS and best prediction from auxes
- ranking of auxes correlation and top ten

#### OUTPUTS:

- regression error reduction
- list of most correlated signals
- Goodness Of Fit for the regeression model made with the selected set of aux channels
(- elapsed time of the script)

--------------------------------------------

#### Recent upgrades:

- Adapted to run on Virgo data (jose)
- "quick and rough" gps calculator: you give the day and the time and it authomatically
  computes the gps time (fra) 
- Preliminary quality checks on target signal and auxiliary channels added (fra)
- New support for piecewise constant signal: previously the downsampling process
  introduced artifact oscillations at transitions (fra)
- Added "verbose" auxiliary channel plotting for inspection and channel quality (fra)
- Better band-pass filtering (w/o artifacts) for narrow-bands (jose, still in progress)
- Added ranking options and plotting (fra)
- Fixed bugs in the ranking (fra)
- GOF (fra)


-------------------------------------------

### To-do list

- Make spectrogram of target blrms and auxiliary channels (suggested by drago)
- Compute subtraction
- Implement the tracking of the maxima for drifting lines investigation (fra,  bas)
