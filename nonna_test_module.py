from scipy.signal import butter, lfilter, filtfilt, freqz
from numpy import mean, square, var, resize

### List of filters that will be used in the analysis

def butter_bandpass(lowcut, highcut, fs, order=5):
 	nyq = 0.5 * fs
	low = lowcut / nyq
	high = highcut / nyq
	[b, a] = butter(order, [low, high], btype='band')
	return b, a

def butter_lowpass(highcut, fs, order=5):
 	nyq = 0.5 * fs
	#low = lowcut / nyq
	high = highcut / nyq
	[b, a] = butter(order, high, btype='low')
    	return b, a

def butter_highpass(lowcut, fs, order=5):
 	nyq = 0.5 * fs
	low = lowcut / nyq
	#high = highcut / nyq
	[b, a] = butter(order, low, btype='high')
    	return b, a


def butter_bnf_filter(data, lowcut, highcut, fs, order=5,btype='band'):
	if btype == 'band':
		b, a = butter_bandpass(lowcut, highcut, fs, order=order)
		#y = lfilter(b, a, data)
		y = filtfilt(b, a, data) # Back and forward filter: this has twice the order of a standard filter
    		return y
	elif btype == 'low':
		b, a = butter_lowpass(highcut, fs, order=order)
		#y = lfilter(b, a, data)
		y = filtfilt(b, a, data)
		return y
	elif btype == 'high':
		b, a = butter_highpass(lowcut, fs, order=order)
		#y = lfilter(b, a, data)
		y = filtfilt(b, a, data)
		return y

### Regression error

def rerror(X,p,y):
	e = mean(square(X*p-resize(y,(len(y),1)))) # y has been transformed into a (len(y),1) matrix to make the subtraction
	R2 = 1-(e/var(y))
	return e, R2
